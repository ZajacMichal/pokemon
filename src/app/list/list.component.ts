import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PokemonService } from '../pokemon.service';
import { CardDetailsComponent } from '../card-details/card-details.component';
import { MatDialog } from '@angular/material';
import { PokemonCard } from '../pokemon.interface';
import { ScrollDispatcher, CdkScrollable } from '@angular/cdk/scrolling';
import { filter } from 'rxjs/operators';

import chunk from 'lodash/chunk';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  private page = 0;
  public limitReached: boolean;
  public cards: PokemonCard[];
  public isLoading: boolean;
  get displayCards(): PokemonCard[][] {
    if (!(this.cards && this.cards.length)) {
      return [];
    }
    return chunk(this.cards, 3);
  }

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly pokemonService: PokemonService,
    private readonly dialog: MatDialog,
    private readonly cd: ChangeDetectorRef,
    private readonly sd: ScrollDispatcher
  ) { }

  ngOnInit() {
    this.listenForParamChanges();
    this.getCards();
    this.sd
      .scrolled()
      .pipe(
        filter(
          scrollable =>
            !!scrollable &&
            scrollable.measureScrollOffset('bottom') < 1500 &&
            !this.isLoading &&
            !this.limitReached
        )
      )

      .subscribe((res: CdkScrollable) => {
        this.loadMore();
      });
  }

  /**
   * Pobiera karty z podanym parametrem
   *
   * @returns void
   */
  getCards(): void {
    this.isLoading = true;
    this.pokemonService.getCards(this.page).subscribe(loadedCards => {
      this.cards = loadedCards;
      this.isLoading = false;
    });
  }

  /**
   * Doładowuje kolejne karty do widoku
   *
   * @returns void
   */
  loadMore(): void {
    this.isLoading = true;
    this.pokemonService.getCards(++this.page).subscribe(newCards => {
      this.cards = [...(this.cards || []), ...newCards];
      this.isLoading = false;
      this.cd.detectChanges();
      if (newCards.length === 0) {
        this.limitReached = true;
      }
    });
  }

  /**
   * Nasłuchuje na zmiany parametrów dla routingu
   *
   * @returns void
   */
  listenForParamChanges(): void {
    this.activatedRoute.params.subscribe(params => {
      if ('id' in this.activatedRoute.snapshot.params) {
        const id = this.activatedRoute.snapshot.params.id;
        this.loadPokemonDetails(id);
      }
    });
  }

  /**
   * Pobiera dane na temat pokemona
   *
   * @param  {string} id
   * @returns void
   */
  loadPokemonDetails(id: string): void {
    this.pokemonService.getPokemonById(id).subscribe(res => {
      this.openCardDetails(res);
    });
  }

  /**
   * Otwiera dialog z opisem pokemona
   *
   * @param  {PokemonCard} card
   * @returns void
   */
  openCardDetails(card: PokemonCard): void {
    const dialogRef = this.dialog.open(CardDetailsComponent, {
      height: '100vh',
      data: card
    });

    dialogRef.afterClosed().subscribe(result => {
      this.router.navigate(['/']);
    });
  }
}
