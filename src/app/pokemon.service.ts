import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PokemonCard, IPokemonApiResponse, IPokemonGetCardByIdResponse } from './pokemon.interface';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  public pokemonApi = 'https://api.pokemontcg.io/v1/cards';
  constructor(private http: HttpClient) {}

  /**
   * Pobiera karty pokemonów o podanych parametrach
   *
   * @param  {number=0} page
   * @returns Observable
   */
  getCards(page: number = 0): Observable<PokemonCard[]> {
    const params = {
      pageSize: '20',
      page: page.toString()
    };

    return this.http
      .get<IPokemonApiResponse>(this.pokemonApi, { params })
      .pipe(map(response => response.cards));
  }

  /**
   * Pobiera pokemony podobne do aktualnego (określone parametry)
   *
   * @param  {PokemonCard} card
   * @returns Observable
   */
  getSimiliarPokemons(card: PokemonCard): Observable<PokemonCard[]> {
    const params = {
      pageSize: '4',
      types: card.types ? card.types.join('|') : '',
      rarity: card.rarity,
      // Api zwraca 500 w momencie połączenia warunków gt i lt, dlatego zostawiam jedynie "greater than"
      hp: typeof card.hp === 'number' ? 'gt' + Math.floor(parseInt(card.hp, 10) * 0.9) : ''
    };
    return this.http
      .get<IPokemonApiResponse>(this.pokemonApi, { params })
      .pipe(map(response => response.cards
        ));
  }

  /**
   * Pobiera pokemona o przekazanym id
   *
   * @param  {string} id
   * @returns Observable
   */
  getPokemonById(id: string): Observable<PokemonCard> {
    const url = 'https://api.pokemontcg.io/v1/cards';
    return this.http
      .get<IPokemonGetCardByIdResponse>(url + '/' + id)
      .pipe(map(response => response.card));
  }
}
