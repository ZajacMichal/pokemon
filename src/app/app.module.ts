import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { MatDialogModule, MatToolbarModule, MatProgressBarModule } from '@angular/material';
import { CardDetailsComponent } from './card-details/card-details.component';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { ListComponent } from './list/list.component';
import { RouteReuseStrategy } from '@angular/router';
import { ReuseStrategy } from './route-reuse';

@NgModule({
  declarations: [AppComponent, CardDetailsComponent, ListComponent],
  entryComponents: [CardDetailsComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatDialogModule,
    ScrollingModule,
    MatToolbarModule,
    MatProgressBarModule
  ],
  exports: [AppComponent],
  providers: [{ provide: RouteReuseStrategy, useClass: ReuseStrategy }],
  bootstrap: [AppComponent]
})
export class AppModule {}
