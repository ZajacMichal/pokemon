import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PokemonCard } from '../pokemon.interface';
import { PokemonService } from '../pokemon.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-card-details',
  templateUrl: './card-details.component.html',
  styleUrls: ['./card-details.component.scss']
})
export class CardDetailsComponent implements OnInit {
  similarPokemons$: Observable<PokemonCard[]>;

  constructor(
    public dialogRef: MatDialogRef<CardDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public card,
    private readonly pokemonService: PokemonService
  ) {}

  ngOnInit() {
    this.similarPokemons$ = this.pokemonService.getSimiliarPokemons(this.card);
  }
}
