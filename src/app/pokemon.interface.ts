export interface IPokemonApiResponse {
  cards: PokemonCard[];
}

export interface IPokemonGetCardByIdResponse {
  card: PokemonCard;
}

export interface PokemonCard {
  artist: string;
  attacks: Attacks[];
  convertedRetreatCost: number;
  hp: string;
  id: string;
  imageUrl: string;
  imageUrlHiRes: string;
  name: string;
  nationalPokedexNumber: number;
  number: string;
  rarity: string;
  resistances: ResistancesAndWeaknesses[];
  retreatCost: string[];
  series: string;
  set: string;
  setCode: string;
  subtype: string;
  supertype: string;
  types: string[];
  weaknesses: ResistancesAndWeaknesses[];
}

export interface Attacks {
  convertedEnergyCost: number;
  cost: string[];
  damage: string;
  name: string;
  text: string;
}

export interface ResistancesAndWeaknesses {
  type: string;
  value: string;
}
